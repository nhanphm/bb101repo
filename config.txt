Now, you have finished the simplest workflow between Bitbucket and a DVCS system. You created a local repository, added a new file to it, and pushed those changes to the Bitbucket repository. At this point you may have a few thoughts in your head such as:

Hey, I don't consider a README file to be source code.
Aren't other people supposed to help me with my repository?
This Git stuff is fine but I want to use Mercurial.
The next page of this tutorial will resolve some of these thoughts because you will use the fork operation and use Mercurial to add code to another user's repository.